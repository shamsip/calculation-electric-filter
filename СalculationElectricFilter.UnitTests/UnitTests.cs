﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Library;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Diagnostics;
using Microsoft.Office.Interop.Excel;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTests
    {
        /// <summary>
        /// Имя файла Microsoft Excel
        /// </summary>
        static string filename = "Calculation.xlsx";

        /// <summary>
        /// Экземпляр класса подключения к Miscrosoft Excel
        /// </summary>
        ExcelClass test = new ExcelClass(filename);

        /// <summary>
        /// Экземпляр класса математической библиотеки
        /// </summary>
        EstimatedMethods EstimatedSample = new EstimatedMethods();

        // ИСХОДНЫЕ ЗНАЧЕНИЯ
        // --------------------------------------------
        static double V_RO = 108000;  // Расход сухого газа на очистку при нормальных условиях, м3/ч 
        static double X = 100;     // Содержание водяных паров в газе, г/м3
        static double T_GAZ = 300; // Температура очищаемого газа на входе, C
        static double P_BAR = 101.5; // Барометрическое давление на местности, кПа
        static double P_GI = 9.5;  // Избыточное давление очищаемого газа, кПа
        static double W = 0.8;     // Рекомендуемая скорость очищаемого газа, м/c
        static double I = 0.22; // Заданный средний ток короны
        static double Z = 30; // Концентрация пыли на входе
        static double D_V = Math.Pow(2.2, -5); // Динамическая вязкость газа при рабочих условиях
        static double P_EL = 100000000; // Удельное электрическое сопротивление слоя пыли
        static List<double> Q_J = new List<double>() { 2, 10, 15, 25, 50, 75 }; // Фракционный состав пыли

        Сalculation CalculationSample = new Сalculation(V_RO, X, T_GAZ, P_BAR, P_GI, W, Z, I, Q_J, D_V, P_EL);

        /// <summary>
        /// Получение значений всех переменных статичного класса для дальнейшего сравнения их в тестах
        /// </summary>
        [TestInitialize]
        public void TestInit()
        {
            CalculationSample.GetAllValues();
        }

        // Расход очищаемого газа в рабочих условиях (V_GAZ)
        [TestMethod]
        public void TestExpenditureAmount()
        {
            double expected = test.ExcelIntermediatetData[0];
            double result = CalculationSample.V_GAZ;

            Assert.AreEqual(expected, result);
        }

        // Площадь осаждения (F_PA)
        [TestMethod]
        public void TestDepositionArea()
        {
            double expected = test.ExcelIntermediatetData[1];
            double result = CalculationSample.F_PA;

            Assert.AreEqual(expected, result);
        }

        // Выбор типа элекрофильтра
        [TestMethod]
        public void TestSelectedFilter()
        {
            object[] FilterValues = {
                    CalculationSample.FMark, // Марка электрофильтра
                    CalculationSample.N_CK, // Число секций
                    CalculationSample.N_G,  // Число газовых проходов
                    CalculationSample.A_H,  // Активная высота электродов
                    CalculationSample.A_L,  // Активная длина поля
                    CalculationSample.N_P,  // Число полей
                    CalculationSample.F_A,  // Площадь активного сечения
                    CalculationSample.HK,   // Шаг между одноимёнными электродами
                    CalculationSample.F_OC, // Общая площадь осаждения
                    CalculationSample.F_L, // Длина
                    CalculationSample.F_H, // Высота
                    CalculationSample.F_B, // Ширина
                    CalculationSample.R,  // Эффективный радиус коронирующего электрода
                    CalculationSample.W_U, // Уточненная скорость газа в электрофильтре
                    CalculationSample.COR_EL // Тип коронирующего электрода
                };

            var expected = test.ExcelFilterData;
            var result = FilterValues;

            CollectionAssert.AreEqual(expected, result);
        }

        // Расстояние между коронирующим и осадительным электродами (HP)
        [TestMethod]
        public void TestDistanceBetweenElectrodes()
        {
            double expected = test.ExcelFilterParametersData[0];
            double result = CalculationSample.HP;

            Assert.AreEqual(expected, result);
        }

        // Удельная осадительная поверхность (F_F)
        [TestMethod]
        public void TestPrecipitationSurface()
        {

            double expected = test.ExcelFilterParametersData[1];
            double result = CalculationSample.F_F;

            Assert.AreEqual(expected, result);
        }

        // Время пребывания газа в электрофильтре (T_T)
        [TestMethod]
        public void TestGasResidenceTime()
        {
            double expected = test.ExcelFilterParametersData[2];
            double result = CalculationSample.T_T;

            Assert.AreEqual(expected, result);
        }

        // Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности (E_KP)
        [TestMethod]
        public void TestCriticalFieldOfNegativeElectrode()
        {
            double expected = test.ExcelFilterParametersData[3];
            double result = CalculationSample.E_KP;

            Assert.AreEqual(expected, result);
        }

        // Критическое напряжение электрического поля у коронирующего электрода (U_KP)
        [TestMethod]
        public void TestCriticaVoltageOfElectrode()
        {
            double expected = test.ExcelFilterParametersData[4];
            double result = CalculationSample.U_KP;

            Assert.AreEqual(expected, result);
        }

        // Напряженность электрического поля вблизи поверхности осаждения пыли (E_OC)
        [TestMethod]
        public void TestElectricFieldOfDepositionSurface()
        {
            double expected = test.ExcelFilterParametersData[5];
            double result = CalculationSample.E_OC;

            Assert.AreEqual(expected, result);
        }

        // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций (W_W)
        [TestMethod]
        public void TestPhysicalVelocityOnAnalyticalOfFractions()
        {
            List<double> expected = new List<double>();

            for (int i = 6; i < 12; i++)
            {
                expected.Add(test.ExcelFilterParametersData[i]);
            }

            List<double> result = CalculationSample.W_W;

            CollectionAssert.AreEqual(expected, result);
        }

        // Фракционная степень очистки газа (Z_Z)
        [TestMethod]
        public void TestFractionalDegreeOfPurification()
        {
            List<double> expected = new List<double>();

            for (int i = 12; i < 18; i++)
            {
                expected.Add(test.ExcelFilterParametersData[i]);
            }

            List<double> result = CalculationSample.Z_Z;

            CollectionAssert.AreEqual(expected, result);
        }

        // Общая аналитическая степень очистки газа (ZZ_A)
        [TestMethod]
        public void TestGeneralAnalyticalDegreeOfPurification()
        {
            double expected = test.ExcelFilterParametersData[18];
            double result = CalculationSample.ZZ_A;

            Assert.AreEqual(expected, result);
        }

        // Физическая скорость дрейфа частиц (W_E)
        [TestMethod]
        public void TestPhysicalSpeedOfParticles()
        {
            double expected = test.ExcelFilterParametersData[19];
            double result = CalculationSample.W_E;

            Assert.AreEqual(expected, result);
        }

        // Количество осаждаемой пыли (M_OC)
        [TestMethod]
        public void TestAmountPrecipitationOfDust()
        {
            double expected = test.ExcelFilterParametersData[20];

            // В данном случае округление оправдано лимитом знаков в Microsoft Excel
            double result = Math.Round(CalculationSample.M_OC, 11);

            Assert.AreEqual(expected, result);
        }

        // Количество уносимой из фильтра пыли (M_YH)
        [TestMethod]
        public void TestAmountRemovedOfDust()
        {
            // В данном случае округление оправдано различием на 1 миллионую
            double expected = Math.Round(test.ExcelFilterParametersData[21], 11);
            double result = Math.Round(CalculationSample.M_YH, 11);

            Assert.AreEqual(expected, result);
        }

        // Рациональная пылеемкость осадительных электродов (M_P)
        [TestMethod]
        public void TestRationalDustOfElectrodes()
        {
            double expected = test.ExcelFilterParametersData[22];
            double result = CalculationSample.M_P;

            Assert.AreEqual(expected, result);
        }

        // Рекомендуемое время между очередным встряхиванием осадительных электродов (T_P)
        [TestMethod]
        public void TestRecommendedTimeBetweenShakeOfElectrodes()
        {
            double expected = test.ExcelFilterParametersData[23];
            double result = CalculationSample.T_P;

            Assert.AreEqual(expected, result);
        }

        // Сила тока, необходимая на питание одного поля электрофильтра (I_CUR)
        [TestMethod]
        public void TestRequiredCurrentToPowerOneFieldOfFilter()
        {
            double expected = test.ExcelFilterParametersData[24];
            double result = CalculationSample.I_CUR;

            Assert.AreEqual(expected, result);
        }

        // Выбор агрегата питанимя
        [TestMethod]
        public void TestSelectPowerUnit()
        {
            object[] FilterValues =
            {
                    CalculationSample.PMark, // Марка питания агрегата
                    CalculationSample.U_MAX,  // Выпрямленное напряжение (максимальное)
                    CalculationSample.U_U,    // Выпрямленное напряжение (среднее)
                    CalculationSample.I_L,    // Выпрямленный ток 
                    CalculationSample.I_P,    // Потребляемый ток 
                    CalculationSample.U_C,    // Напряжение сети 
                    CalculationSample.N_C,    // Потребляемая из сети мощность
                    CalculationSample.P_AG,   // КПД агрегата питания
                    CalculationSample.K_N    // Коэффициент мощности
                };

            var expected = test.ExcelPowerUnitData;
            var result = FilterValues;

            CollectionAssert.AreEqual(expected, result);
        }

        // Расчетная потребляемая мощность агрегата на очистку газа (N_R)
        [TestMethod]
        public void TestEstimatePowerOfConsumption()
        {
            double expected = test.ExcelPowerUnitParametersData[0];
            double result = CalculationSample.N_R;

            Assert.AreEqual(expected, result);
        }

        // Общая мощность, потребляемая агрегатом питания (N_S)
        [TestMethod]
        public void TestTotalPowerOfConsumption()
        {
            double expected = test.ExcelPowerUnitParametersData[1];
            double result = CalculationSample.N_S;

            Assert.AreEqual(expected, result);
        }

        // Количество агрегатов питания на электрофильтр (N_A)
        [TestMethod]
        public void TestNumberOfAggregates()
        {
            double expected = test.ExcelPowerUnitParametersData[2];
            double result = CalculationSample.N_A;

            Assert.AreEqual(expected, result);
        }
    }
}
