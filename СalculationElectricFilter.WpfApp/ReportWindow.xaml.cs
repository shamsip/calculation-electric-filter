﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace СalculationElectricFilter.WpfApp
{
    /// <summary>
    /// Логика взаимодействия для Report.xaml
    /// </summary>
    public partial class ReportWindow : Window
    {
        private System.ComponentModel.IContainer components = null;
        public System.Windows.Forms.BindingSource DataBinding_Source;
        public System.Windows.Forms.BindingSource DataBinding_Intermediate;
        public System.Windows.Forms.BindingSource DataBinding_TypeOfElectricFilter;
        public System.Windows.Forms.BindingSource DataBinding_CalculationOfElectricFilter;
        public System.Windows.Forms.BindingSource DataBinding_TypeOfPowerUnit;
        public System.Windows.Forms.BindingSource DataBinding_CalculationOfPowerUnit;

        public ReportWindow()
        {

            InitializeComponent();

            this.components = new System.ComponentModel.Container();

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource_Source = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource_Intermediate = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource_TypeOfElectricFilter = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource_CalculationOfElectricFilter = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource_TypeOfPowerUnit = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource_CalculationOfPowerUnit = new Microsoft.Reporting.WinForms.ReportDataSource();

            this.DataBinding_Source = new System.Windows.Forms.BindingSource(this.components);
            this.DataBinding_Intermediate = new System.Windows.Forms.BindingSource(this.components);
            this.DataBinding_TypeOfElectricFilter = new System.Windows.Forms.BindingSource(this.components);
            this.DataBinding_CalculationOfElectricFilter = new System.Windows.Forms.BindingSource(this.components);
            this.DataBinding_TypeOfPowerUnit = new System.Windows.Forms.BindingSource(this.components);
            this.DataBinding_CalculationOfPowerUnit = new System.Windows.Forms.BindingSource(this.components);

            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_Source)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_Intermediate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_TypeOfElectricFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_CalculationOfElectricFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_TypeOfPowerUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_CalculationOfPowerUnit)).BeginInit();

            reportDataSource_Source.Name = "DataSource_Source";
            reportDataSource_Source.Value = this.DataBinding_Source;

            reportDataSource_Intermediate.Name = "DataSource_Intermediate";
            reportDataSource_Intermediate.Value = this.DataBinding_Intermediate;

            reportDataSource_TypeOfElectricFilter.Name = "DataSource_TypeOfElectricFilter";
            reportDataSource_TypeOfElectricFilter.Value = this.DataBinding_TypeOfElectricFilter;

            reportDataSource_CalculationOfElectricFilter.Name = "DataSource_CalculationOfElectricFilter";
            reportDataSource_CalculationOfElectricFilter.Value = this.DataBinding_CalculationOfElectricFilter;

            reportDataSource_TypeOfPowerUnit.Name = "DataSource_TypeOfPowerUnit";
            reportDataSource_TypeOfPowerUnit.Value = this.DataBinding_TypeOfPowerUnit;

            reportDataSource_CalculationOfPowerUnit.Name = "DataSource_CalculationOfPowerUnit";
            reportDataSource_CalculationOfPowerUnit.Value = this.DataBinding_CalculationOfPowerUnit;

            this.ReportViewer.LocalReport.DataSources.Add(reportDataSource_Source);
            this.ReportViewer.LocalReport.DataSources.Add(reportDataSource_Intermediate);
            this.ReportViewer.LocalReport.DataSources.Add(reportDataSource_TypeOfElectricFilter);
            this.ReportViewer.LocalReport.DataSources.Add(reportDataSource_CalculationOfElectricFilter);
            this.ReportViewer.LocalReport.DataSources.Add(reportDataSource_TypeOfPowerUnit);
            this.ReportViewer.LocalReport.DataSources.Add(reportDataSource_CalculationOfPowerUnit);

            this.ReportViewer.LocalReport.ReportEmbeddedResource = "СalculationElectricFilter.WpfApp.Report.rdlc";

            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_Source)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_Intermediate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_TypeOfElectricFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_CalculationOfElectricFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_TypeOfPowerUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataBinding_CalculationOfPowerUnit)).EndInit();

        }

        private void Window_Loaded_2(object sender, RoutedEventArgs e)
        {
            this.ReportViewer.RefreshReport();
        }
    }
}
