﻿using Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace СalculationElectricFilter.WpfApp
{

    public static class ReportInitializationClass
    {
        /// <summary>
        /// ПОЛУЧЕНИЕ ТЕКСТВОГО ЗНАЧЕНИЯ ИЗ КОЛЛЕКЦИЙ (ДЛЯ ОТЧЁТА)
        /// </summary>
        /// <returns></returns>
        public static string GetConvertedValuesInCollection(List<double> Collection)
        {
            string converted = "";

            for (int i = 0; i < 6; i++)
            {
                converted += " " + Collection[i].ToString();
            }

            return converted;
        }

        /// <summary>
        /// НАСТРОЙКА ОТЧЁТА
        /// </summary>
        public static void ReportСonfigure()
        {
            ReportWindow RV_Form = new ReportWindow();

            List<ViewerClass> RepListSource = new List<ViewerClass>();
            List<ViewerClass> RepListIntermediate = new List<ViewerClass>();
            List<ViewerClass> RepListTypeOfElectricFilter = new List<ViewerClass>();
            List<ViewerClass> RepListCalculationOfElectricFilter = new List<ViewerClass>();
            List<ViewerClass> RepListTypeOfPowerUnit = new List<ViewerClass>();
            List<ViewerClass> RepListCalculationOfPowerUnit = new List<ViewerClass>();

            MainWindow MWSample = new MainWindow();

            MWSample.GetSourceValues();
            MWSample.CalculationSample.GetAllValues();
            try
            {
                RepListSource.Add(new ViewerClass("Расход сухого газа на очистку при нормальных условиях, м3/ч", MWSample.CalculationSample.V_RO.ToString()));
                RepListSource.Add(new ViewerClass("Содержание водяных паров в газе, г/м3", MWSample.CalculationSample.X.ToString()));
                RepListSource.Add(new ViewerClass("Температура очищаемого газа на входе, C", MWSample.CalculationSample.T_GAZ.ToString()));
                RepListSource.Add(new ViewerClass("Барометрическое давление на местности, кПа", MWSample.CalculationSample.P_BAR.ToString()));
                RepListSource.Add(new ViewerClass("Рекомендуемая скорость очищаемого газа, м/с", MWSample.CalculationSample.W.ToString()));
                RepListSource.Add(new ViewerClass("Заданный средний ток короны, мА/м", MWSample.CalculationSample.I.ToString()));
                RepListSource.Add(new ViewerClass("Концентрация пыли на входе, г/м3", MWSample.CalculationSample.Z.ToString()));
                RepListSource.Add(new ViewerClass("Динамическая вязкость газа при рабочих условиях, Па * с", MWSample.CalculationSample.D_V.ToString("0.00000")));
                RepListSource.Add(new ViewerClass("Удельное электрическое сопротивление слоя пыли, Ом * м", MWSample.CalculationSample.P_EL.ToString()));

                //
                RepListSource.Add(new ViewerClass("Фракционный состав пыли, мкм", GetConvertedValuesInCollection(MWSample.CalculationSample.Q_J).ToString()));

                RepListIntermediate.Add(new ViewerClass("Расход очищаемого газа в рабочих условиях м3/ч", MWSample.CalculationSample.V_GAZ.ToString("0.000")));
                RepListIntermediate.Add(new ViewerClass("Площадь активного сечения электрофильтра, м2", MWSample.CalculationSample.F_PA.ToString("0.000")));

                RepListTypeOfElectricFilter.Add(new ViewerClass("Марка электрического фильтра", MWSample.CalculationSample.FMark.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Число секций", MWSample.CalculationSample.N_CK.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Число газовых проходов", MWSample.CalculationSample.N_G.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Активная высота электродов, м", MWSample.CalculationSample.A_H.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Активная длина поля, м", MWSample.CalculationSample.A_L.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Число полей", MWSample.CalculationSample.N_P.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Площадь активного сечения, м", MWSample.CalculationSample.F_A.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Шаг между электродами, м", MWSample.CalculationSample.HK.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Общая площадь осаждения, м2", MWSample.CalculationSample.F_OC.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Длина электрического фильтра, м", MWSample.CalculationSample.F_L.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Высота электрического фильтра, м", MWSample.CalculationSample.F_H.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Ширина электрического фильтра, м", MWSample.CalculationSample.F_B.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Эффективный радиус коронирующего электрода, м", MWSample.CalculationSample.R.ToString()));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Уточнённая скорость газа в электрофильтре, м/с", MWSample.CalculationSample.W_U.ToString("0.000")));
                RepListTypeOfElectricFilter.Add(new ViewerClass("Тип коронирующего электрода", MWSample.CalculationSample.COR_EL));

                RepListCalculationOfElectricFilter.Add(new ViewerClass("Расстояние между коронирующим и осадительным электродами, м", MWSample.CalculationSample.HP.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Удельная осадительная поверхность, c/м", MWSample.CalculationSample.F_F.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Время пребывания газа в электрофильтре, с", MWSample.CalculationSample.T_T.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности, В/м", MWSample.CalculationSample.E_KP.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Критическое напряжение электрического поля у коронирующего электрода, В", MWSample.CalculationSample.U_KP.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Напряженность электрического поля вблизи поверхности осаждения пыли, В/м", MWSample.CalculationSample.E_OC.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций, м/c", ""));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Физическая скорость дрейфа [1]", MWSample.CalculationSample.W_W[0].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Физическая скорость дрейфа [2]", MWSample.CalculationSample.W_W[1].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Физическая скорость дрейфа [3]", MWSample.CalculationSample.W_W[2].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Физическая скорость дрейфа [4]", MWSample.CalculationSample.W_W[3].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Физическая скорость дрейфа [5]", MWSample.CalculationSample.W_W[4].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Физическая скорость дрейфа [6]", MWSample.CalculationSample.W_W[5].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Фракционная степень очистки газа", ""));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Фракционная степень очистки газа [1]", MWSample.CalculationSample.Z_Z[0].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Фракционная степень очистки газа [2]", MWSample.CalculationSample.Z_Z[1].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Фракционная степень очистки газа [3]", MWSample.CalculationSample.Z_Z[2].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Фракционная степень очистки газа [4]", MWSample.CalculationSample.Z_Z[3].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Фракционная степень очистки газа [5]", MWSample.CalculationSample.Z_Z[4].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Фракционная степень очистки газа [6]", MWSample.CalculationSample.Z_Z[5].ToString("0.0000000000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Общая аналитическая степень очистки газа", MWSample.CalculationSample.ZZ_A.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Физическая скорость дрейфа частиц, м/c", MWSample.CalculationSample.W_E.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Количество осаждаемой пыли, г/ч", MWSample.CalculationSample.M_OC.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Количество уносимой пыли, г/ч", MWSample.CalculationSample.M_YH.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Рациональная пылеёмкость осадительных электродов, кг/м3", MWSample.CalculationSample.M_P.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Рекомендуемое время между очередным встряхиванием осадительных электродов, ч", MWSample.CalculationSample.T_P.ToString("0.000")));
                RepListCalculationOfElectricFilter.Add(new ViewerClass("Сила тока, необходимая на питания одного поля электрофильтра, А", MWSample.CalculationSample.I_CUR.ToString("0.000")));

                RepListTypeOfPowerUnit.Add(new ViewerClass("Марка агрегата питания", MWSample.CalculationSample.PMark));
                RepListTypeOfPowerUnit.Add(new ViewerClass("Максимальное выпрямленное напряжение, В", MWSample.CalculationSample.U_MAX.ToString()));
                RepListTypeOfPowerUnit.Add(new ViewerClass("Среднее выпрямленное напряжение, В", MWSample.CalculationSample.U_U.ToString()));
                RepListTypeOfPowerUnit.Add(new ViewerClass("Выпрямленный ток, А", MWSample.CalculationSample.I_L.ToString()));
                RepListTypeOfPowerUnit.Add(new ViewerClass("Потребляемый ток, А", MWSample.CalculationSample.I_P.ToString()));
                RepListTypeOfPowerUnit.Add(new ViewerClass("Напряжение сети, В", MWSample.CalculationSample.U_C.ToString()));
                RepListTypeOfPowerUnit.Add(new ViewerClass("Потребляемая из сети мощность, кВа", MWSample.CalculationSample.N_C.ToString()));
                RepListTypeOfPowerUnit.Add(new ViewerClass("КПД агрегата питания", MWSample.CalculationSample.P_AG.ToString()));
                RepListTypeOfPowerUnit.Add(new ViewerClass("Коэффициент мощности", MWSample.CalculationSample.K_N.ToString()));

                RepListCalculationOfPowerUnit.Add(new ViewerClass("Расчётная потребляемая мощность агрегата на очистку газа, кВа", MWSample.CalculationSample.N_R.ToString("0.0000")));
                RepListCalculationOfPowerUnit.Add(new ViewerClass("Общая мощность, потребляемая агрегатом питания, кВа", MWSample.CalculationSample.N_S.ToString("0.0000")));
                RepListCalculationOfPowerUnit.Add(new ViewerClass("Количество агрегатов питания на электрофильтр", MWSample.CalculationSample.N_A.ToString()));
            }
            catch
            {

            }

            RV_Form.DataBinding_Source.DataSource = RepListSource;
            RV_Form.DataBinding_Intermediate.DataSource = RepListIntermediate;
            RV_Form.DataBinding_TypeOfElectricFilter.DataSource = RepListTypeOfElectricFilter;
            RV_Form.DataBinding_CalculationOfElectricFilter.DataSource = RepListCalculationOfElectricFilter;
            RV_Form.DataBinding_TypeOfPowerUnit.DataSource = RepListTypeOfPowerUnit;
            RV_Form.DataBinding_CalculationOfPowerUnit.DataSource = RepListCalculationOfPowerUnit;

            RV_Form.Show();
        }
    }
}
