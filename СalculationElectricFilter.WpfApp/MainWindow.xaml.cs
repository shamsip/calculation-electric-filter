﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Library;

namespace СalculationElectricFilter.WpfApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Сalculation CalculationSample;

        /// <summary>
        /// ИНИЦИАЛИЗАЦИЯ ОТЧЁТА
        /// </summary>
        public void ReportInitializate()
        {
            try
            {
                GetSourceValues();

                Serialize();

                CalculationSample.GetAllValues();

                GetAssigningValues();

                ReportInitializationClass.ReportСonfigure();
            }
            catch
            {

            }
        }

        // ВАЛИДАЦИЯ ДАННЫХ
        public int DS_Count(string s)
        {
            string substr = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString();
            int count = (s.Length - s.Replace(substr, "").Length) / substr.Length;
            return count;
        }

        public void TextboxValidation(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !((Char.IsDigit(e.Text, 0) || ((e.Text == System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString()) && (DS_Count(((TextBox)sender).Text) < 1))));
        }

        public MainWindow()
        {
            InitializeComponent();

            foreach (TextBox t in Grid.Children.OfType<TextBox>())
            {
                t.PreviewTextInput += TextboxValidation;
            }

            Deserialize();
        }

        public void Deserialize()
        {
            try
            {
                JsonSerializer formatter = new JsonSerializer();
                using (StreamReader fs = new StreamReader("InputData.txt"))
                {
                    Dictionary<string, object> Data = (Dictionary<string, object>)formatter.Deserialize(fs, typeof(Dictionary<string, object>));

                    textBox_V_RO.Text = Convert.ToDouble(Data["V_RO"]).ToString();
                    textBox_X.Text = Convert.ToDouble(Data["X"]).ToString();
                    textBox_T_GAZ.Text = Convert.ToDouble(Data["T_GAZ"]).ToString();
                    textBox_P_BAR.Text = Convert.ToDouble(Data["P_BAR"]).ToString();
                    textBox_P_GI.Text = Convert.ToDouble(Data["P_GI"]).ToString();
                    textBox_W.Text = Convert.ToDouble(Data["W"]).ToString();
                    textBox_I.Text = Convert.ToDouble(Data["I"]).ToString();
                    textBox_Z.Text = Convert.ToDouble(Data["Z"]).ToString();

                    foreach (double value in ((JArray)Data["Q_J"]).ToObject<List<double>>())
                    {
                        textBox_YJ.Text += value.ToString() + " ";
                    }

                    // Удаление последнего пробела из текстбокса
                    textBox_YJ.Text = textBox_YJ.Text.Substring(0, textBox_YJ.Text.Length - 1);

                    textBox_DV.Text = Convert.ToDouble(Data["D_V"]).ToString();
                    textBox_PEL.Text = Convert.ToDouble(Data["P_EL"]).ToString();
                }
            }
            catch
            {

            }
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            List<double> textBoxesValues = new List<double>();

            try
            {
                textBoxesValues.Add(Convert.ToDouble(textBox_V_RO.Text));
                textBoxesValues.Add(Convert.ToDouble(textBox_X.Text));
                textBoxesValues.Add(Convert.ToDouble(textBox_T_GAZ.Text));
                textBoxesValues.Add(Convert.ToDouble(textBox_P_BAR.Text));
                textBoxesValues.Add(Convert.ToDouble(textBox_P_GI.Text));
                textBoxesValues.Add(Convert.ToDouble(textBox_W.Text));
                textBoxesValues.Add(Convert.ToDouble(textBox_Z.Text));
                textBoxesValues.Add(Convert.ToDouble(textBox_I.Text));
                textBoxesValues.Add(Convert.ToDouble(textBox_YJ.Text));
                textBoxesValues.Add(Convert.ToDouble(textBox_DV.Text));
                textBoxesValues.Add(Convert.ToDouble(textBox_PEL.Text));
            }
            catch
            {
                if (textBoxesValues.Count == 0)
                {
                    GetDeafultValues(); // Вывод исходных значений при запуске первом запуску приложения (до формирования файла сериализации)
                }
            }

            // Вывод версии приложения
            this.Title = this.Title + " [ver. " + Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";
        }

        /// <summary>
        /// РАСЧЁТ
        /// </summary>
        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            List<string> textBoxesValues = new List<string>();

            textBoxesValues.Add(textBox_V_RO.Text);
            textBoxesValues.Add(textBox_X.Text);
            textBoxesValues.Add(textBox_T_GAZ.Text);
            textBoxesValues.Add(textBox_P_BAR.Text);
            textBoxesValues.Add(textBox_P_GI.Text);
            textBoxesValues.Add(textBox_W.Text);
            textBoxesValues.Add(textBox_Z.Text);
            textBoxesValues.Add(textBox_I.Text);
            textBoxesValues.Add(textBox_YJ.Text);
            textBoxesValues.Add(textBox_DV.Text);
            textBoxesValues.Add(textBox_PEL.Text);

            bool IsValuesCorrect = true;
          
            // Исключение "пустых" значений
            foreach (string s in textBoxesValues)
            {
                string[] filterEmpty = s.Split(new char[] { ' ' });
                int counter = 0;
                foreach (string n in filterEmpty)
                {
                    if (n == "")
                    {
                        counter++;
                    }
                }

                if (counter >= 1 || s == String.Empty || s == ",")
                {
                    MessageBox.Show("Поля не должны быть пустыми", "Предупреждение");
                    IsValuesCorrect = false;

                    counter = 0;
                    break;
                }
            }

            // Исключение некорректно введённых значений Фракционного состава пыли
            string[] filter = textBox_YJ.Text.Split(new char[] { ' ' });
            int count = 0;
            foreach (string s in filter)
            {
                count++;
            }

            if (count != 6)
            {
                MessageBox.Show("Фракционный состав пыли должен состоять из шести значений", "Предупреждение");
                IsValuesCorrect = false;
            }

            if (IsValuesCorrect == true)
            {
                // Прокрутка экрана приложения на собственную высоту
                scroll.ScrollToVerticalOffset(scroll.ActualHeight);

                // Появление кнопки прокрутки
                UP.Visibility = Visibility.Visible;

                // Получение значений
                GetSourceValues();

                Serialize();

                CalculationSample.GetAllValues();

                // Очистка коллекции исходных значений Фракционного состава пыли для возможности повторных расчётов
                CalculationSample.Q_J.Clear();

                GetAssigningValues();
            }
        }

        public void Serialize()
        {
            FieldInfo[] fields = CalculationSample.GetFieldInfo();

            Dictionary<string, object> Data = new Dictionary<string, object>();

            foreach (FieldInfo Info in fields)
            {
                Data.Add(Info.Name, Info.GetValue(CalculationSample));
            }

            JsonSerializer formatter = new JsonSerializer();
            using (StreamWriter fs = new StreamWriter("InputData.txt"))
            {
                formatter.Serialize(fs, Data);
            }
        }

        /// <summary>
        /// ПРИСВОЕНИЕ ПЕРЕМЕННЫМ КЛАССА ЗНАЧЕНИЙ ИЗ ФОРМ ВВОДА
        /// </summary>
        public void GetSourceValues()
        {
            // Присвоение переменным исходных значений соответствующих значений
            double V_RO = Convert.ToDouble(textBox_V_RO.Text);
            double X = Convert.ToDouble(textBox_X.Text);
            double T_GAZ = Convert.ToDouble(textBox_T_GAZ.Text);
            double P_BAR = Convert.ToDouble(textBox_P_BAR.Text);
            double P_GI = Convert.ToDouble(textBox_P_GI.Text);
            double W = Convert.ToDouble(textBox_W.Text);
            double Z = Convert.ToDouble(textBox_Z.Text);
            double I = Convert.ToDouble(textBox_I.Text);

            List<double> Q_J = new List<double>();

            string[] filter = textBox_YJ.Text.Split(new char[] { ' ' });
            foreach (string s in filter)
            {
                Q_J.Add(Convert.ToDouble(s));
            }

            double D_V = Convert.ToDouble(textBox_DV.Text);
            double P_EL = Convert.ToDouble(textBox_PEL.Text);

            // Создание экземпляра класса
            CalculationSample = new Сalculation(V_RO, X, T_GAZ, P_BAR, P_GI, W, Z, I, Q_J, D_V, P_EL);
        }

        /// <summary>
        /// ПОЛУЧЕНИЕ ИСХОДНЫХ ЗНАЧЕНИЙ (ДЛЯ КОНТРОЛЬНОГО ПРИМЕРА)
        /// </summary>
        public void GetDeafultValues()
        {
            textBox_V_RO.Text = "108000";
            textBox_X.Text = "100";
            textBox_T_GAZ.Text = "300";
            textBox_P_BAR.Text = "101,5";
            textBox_P_GI.Text = "9,5";
            textBox_W.Text = "0,8";
            textBox_I.Text = "0,22";
            textBox_Z.Text = "30";
            textBox_YJ.Text = "2 10 15 25 50 75";
            textBox_DV.Text = Math.Pow(2.2, -5).ToString();
            textBox_PEL.Text = "100000000";
        }

        /// <summary>
        /// ЗАПИСЬ РАСЧЁТНЫХ ЗНАЧЕНИЙ В ТЕКСТБОКСЫ
        /// </summary>
        public void GetAssigningValues()
        {
            // Промежуточные значения

            textBox_V_GAZ.Text = Math.Round(CalculationSample.V_GAZ, 3).ToString(); // Расход очищаемого газа в рабочих условиях
            textBox_F_PA.Text = Math.Round(CalculationSample.F_PA, 3).ToString();   // Площадь осаждения

            // Значения, относящиеся к электрическому фильтру

            textBox_FMark.Text = CalculationSample.FMark; // Марка электрофильтра
            textBox_NCK.Text = CalculationSample.N_CK.ToString(); // Число секций
            textBox_NG1.Text = CalculationSample.N_G.ToString();  // Число газовых проходов
            textBox_AH.Text = CalculationSample.A_H.ToString();   // Активная высота электродов
            textBox_NP.Text = CalculationSample.N_P.ToString();   // Число полей
            textBox_AL.Text = CalculationSample.A_L.ToString();   // Активная длина поля
            textBox_FA.Text = CalculationSample.F_A.ToString();   // Площадь активного сечения
            textBox_HK.Text = CalculationSample.HK.ToString();    // Шаг между одноимёнными электродами
            textBox_FOC.Text = CalculationSample.F_OC.ToString(); // Общая площадь осаждения
            textBox_FL.Text = CalculationSample.F_L.ToString(); // Длина фильтра
            textBox_FH.Text = CalculationSample.F_H.ToString(); // Высота фильтра
            textBox_FB.Text = CalculationSample.F_B.ToString(); // Ширина фильтра

            textBox_R.Text = CalculationSample.R.ToString(); // Эффективный радиус коронирующего электрода
            textBox_WGE.Text = Math.Round(CalculationSample.W_U, 3).ToString(); // Уточнёная скорость газа в электрофильтре (!)
            textBox_COREL.Text = CalculationSample.COR_EL.ToString(); // Тип коронирующего электрода

            // Параметры электрического фильтра

            textBox_HP.Text = Math.Round(CalculationSample.HP, 3).ToString(); // Расстояние между коронирующим и осадительным электродами
            textBox_FF.Text = Math.Round(CalculationSample.F_F, 3).ToString(); // Удельная осадительная поверхность
            textBox_TT.Text = Math.Round(CalculationSample.T_T, 3).ToString(); // Время пребывания газа в электрофильтре
            textBox_EKP.Text = Math.Round(CalculationSample.E_KP, 3).ToString();  // Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности
            textBox_UKP.Text = Math.Round(CalculationSample.U_KP, 3).ToString();  // Критическое напряжение электрического поля у коронирующего электрода
            textBox_EOC.Text = Math.Round(CalculationSample.E_OC, 3).ToString();  // Напряженность электрического поля вблизи поверхности осаждения пыли

            textBox_WW_1.Text = Math.Round(CalculationSample.W_W[0], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
            textBox_WW_2.Text = Math.Round(CalculationSample.W_W[1], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
            textBox_WW_3.Text = Math.Round(CalculationSample.W_W[2], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
            textBox_WW_4.Text = Math.Round(CalculationSample.W_W[3], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
            textBox_WW_5.Text = Math.Round(CalculationSample.W_W[4], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
            textBox_WW_6.Text = Math.Round(CalculationSample.W_W[5], 10).ToString(); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
    
            textBox_ZZ_1.Text = Math.Round(CalculationSample.Z_Z[0], 10).ToString(); // Фракционная степень очистки газа
            textBox_ZZ_2.Text = Math.Round(CalculationSample.Z_Z[1], 10).ToString(); // Фракционная степень очистки газа
            textBox_ZZ_3.Text = Math.Round(CalculationSample.Z_Z[2], 10).ToString(); // Фракционная степень очистки газа
            textBox_ZZ_4.Text = Math.Round(CalculationSample.Z_Z[3], 10).ToString(); // Фракционная степень очистки газа
            textBox_ZZ_5.Text = Math.Round(CalculationSample.Z_Z[4], 10).ToString(); // Фракционная степень очистки газа
            textBox_ZZ_6.Text = Math.Round(CalculationSample.Z_Z[5], 10).ToString(); // Фракционная степень очистки газа

            textBox_ZZA.Text = Math.Round(CalculationSample.ZZ_A, 3).ToString(); // Общая аналитическая степень очистки газа
            textBox_WE.Text = Math.Round(CalculationSample.W_E, 3).ToString(); // Физическая скорость дрейфа частиц
            textBox_MOC.Text = Math.Round(CalculationSample.M_OC, 3).ToString(); // Количество осаждаемой пыли
            textBox_MYH.Text = Math.Round(CalculationSample.M_YH, 3).ToString(); // Количество уносимой из фильтра пыли
            textBox_MP.Text = Math.Round(CalculationSample.M_P, 3).ToString(); // Рациональная пылеемкость осадительных электродов
            textBox_TP.Text = Math.Round(CalculationSample.T_P, 3).ToString(); // Рекомендуемое время между очередным встряхиванием осадительных электродов
            textBox_ICUR.Text = Math.Round(CalculationSample.I_CUR, 3).ToString(); // Сила тока, необходимая на питание одного поля электрофильтра         

            // Значения, относящиеся к агрегату питания

            textBox_PMark.Text = CalculationSample.PMark; // Марка агрегата питания
            textBox_UMAX.Text = CalculationSample.U_MAX.ToString(); // Максимальное выпрямленное напряжение
            textBox_UU.Text = CalculationSample.U_U.ToString(); // Среднее выпрямленное напряжение
            textBox_IL.Text = CalculationSample.I_L.ToString(); // Выпрямленный ток
            textBox_IP.Text = CalculationSample.I_P.ToString(); // Потребляемый ток                                         
            textBox_UC.Text = CalculationSample.U_C.ToString(); // Напряжение сети
            textBox_NC.Text = CalculationSample.N_C.ToString(); // Потребляемая из сети мощность
            textBox_PAG.Text = CalculationSample.P_AG.ToString(); // КПД агрегата питания
            textBox_KN.Text = CalculationSample.K_N.ToString(); // Коэффициент мощности

            // Параметры агрегата питания

            textBox_NR.Text = Math.Round(CalculationSample.N_R, 3).ToString(); // Расчетная потребляемая мощность агрегата на очистку газа
            textBox_NS.Text = Math.Round(CalculationSample.N_S, 3).ToString(); // Общая мощность, потребляемая агрегатом питания
            textBox_NA.Text = CalculationSample.N_A.ToString();                // Количество агрегатов питания на электрофильтр

        }

        /// <summary>
        /// МЕТОД НАЖАТИЯ НА КНОПКУ "РАСЧЁТ"
        /// </summary>
        private void ReportInit_Click(object sender, RoutedEventArgs e)
        {
            ReportInitializate();
        }

        /// <summary>
        /// МЕТОД ВЫХОДА ИЗ ПРИЛОЖЕНИЯ
        /// </summary>
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            GetSourceValues();

            Serialize();

            Application.Current.Shutdown();
        }

        /// <summary>
        /// МЕТОД НАЖАТИЯ НА КНОПКУ "НАВЕРХ"
        /// </summary>
        private void UP_Click(object sender, RoutedEventArgs e)
        {
            // Прокрутка наверх
            scroll.ScrollToHome();

            // Скрытие кнопки прокрутки
            UP.Visibility = Visibility.Hidden;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            GetSourceValues();

            Serialize();

            Application.Current.Shutdown();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("Руководство пользователя.pdf");
        }
    }
}
