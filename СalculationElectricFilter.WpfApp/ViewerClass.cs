﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace СalculationElectricFilter.WpfApp
{
    public class ViewerClass
    {
        public ViewerClass() { }

        public ViewerClass(string _name, string _value)
        {
            Name = _name;
            Value = _value;
        }


        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _value;
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public string this[int ind]
        {
            get
            {
                switch (ind)
                {
                    case 1:
                        return _name;
                    case 2:
                        return _value;
                    default:
                        return "null";
                }
            }
            set
            {
                switch (ind)
                {
                    case 1:
                        _name = value;
                        break;
                    case 2:
                        _value = value;
                        break;
                    default:
                        break;
                }
            }
        }



    }

    class Source_Data : ViewerClass
    {
    }
    class Intermediate_Data : ViewerClass
    {
    }
    class TypeOfElectricFilter_Data : ViewerClass
    {
    }
    class CalculationOfElectricFilter_Data : ViewerClass
    {
    }
    class TypeOfPowerUnit_Data : ViewerClass
    {
    }
    class CalculationOfPowerUnit_Data : ViewerClass
    {
    }
}