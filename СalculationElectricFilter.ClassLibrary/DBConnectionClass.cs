﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library;

namespace ClassLibrary
{
    public class DBConnectionClass
    {
        // Подключение к базе данных Microsoft Access
        public string connectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Filters.mdb;";
        private OleDbConnection myConnection;

        public void OpenConnection()
        {
            // Инициализация подключения к базе данных Microsoft Excel
            myConnection = new OleDbConnection(connectString);
            myConnection.Open();
        }

        // ЗНАЧЕНИЯ, ОТНОСЯЩИЕСЯ К ТИПУ ЭЛЕКТРОФИЛЬТРА
        // --------------------------------------------
        /// <summary>
        /// Марка электрофильтра
        /// </summary>
        public string FMark;
        /// <summary>
        /// Число секций
        /// </summary>
        public double N_CK;
        /// <summary>
        ///  Число газовых проходов
        /// </summary>
        public double N_G;
        /// <summary>
        /// Активная высота электродов
        /// </summary>
        public double A_H;
        /// <summary>
        /// Число полей
        /// </summary>
        public double N_P;
        /// <summary>
        ///  Активная длина поля
        /// </summary>
        public double A_L;
        /// <summary>
        /// Площадь активного сечения
        /// </summary>
        public double F_A;
        /// <summary>
        /// Общая площадь осаждения
        /// </summary>
        public double F_OC;
        /// <summary>
        /// Длина электрофильтра
        /// </summary>
        public double F_L;
        /// <summary>
        /// Высота электрофильтра
        /// </summary>
        public double F_H;
        /// <summary>
        /// Ширина электрофильтра
        /// </summary>
        public double F_B;
        /// <summary>
        /// Тип коронирующего электрода  
        /// </summary>
        public string COR_EL = "";
        /// <summary>
        /// Шаг между одноимёнными электродами
        /// </summary>
        public double HK;
        /// <summary>
        /// Эффективный радиус коронирующего электрода
        /// </summary>
        public double R;
        /// <summary>
        /// Расстояние между коронирующим и осадительным электродам
        /// </summary>
        public double HP;

        // ЗНАЧЕНИЯ, ОТНОСЯЩИЕСЯ К АГРЕГАТУ ПИТАНИЯ
        // --------------------------------------------
        /// <summary>
        /// Марка питания агрегата
        /// </summary>
        public string PMark;
        /// <summary>
        /// Выпрямленное напряжение (максимальное)
        /// </summary>
        public double U_MAX;
        /// <summary>
        /// Выпрямленное напряжение (среднее)
        /// </summary>
        public double U_U;
        /// <summary>
        /// Выпрямленный ток 
        /// </summary>
        public double I_L;
        /// <summary>
        /// Потребляемый ток 
        /// </summary>
        public double I_P;
        /// <summary>
        /// Напряжение сети
        /// </summary>
        public double U_C;
        /// <summary>
        /// Потребляемая из сети мощность
        /// </summary>
        public double N_C;
        /// <summary>
        /// КПД агрегата питания
        /// </summary>
        public double P_AG;
        /// <summary>
        /// Коэффициент мощности
        /// </summary>
        public double K_N;

        /// <summary>
        /// ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ПО УМОЛЧАНИЮ ДЛЯ ФИЛЬТРОВ МАРКИ "ЭГА"
        /// </summary>
        public void GetDefaultNotationOfIGA()
        {
            // ПРИМЕЧАНИЯ К ЭЛЕКТРОФИЛЬТРАМ МАРКИ "ЭГА"
            HK = 0.3;  // Шаг между одноимёнными электродами
            R = 2 * (Math.Pow(10, -3)); // Эффективный радиус коронирующего электрода, м
            COR_EL = "игольчатый"; // Тип коронирующего электрода
            HP = 0.133; // Расстояние между коронирующим и осадительным электродами
        }
        /// <summary>
        /// ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ПО УМОЛЧАНИЮ ДЛЯ ФИЛЬТРОВ МАРКИ "ЭГТ"
        /// </summary>
        public void GetDefaultNotationOfIGT()
        {
            // ПРИМЕЧАНИЯ К ЭЛЕКТРОФИЛЬТРАМ МАРКИ "ЭГТ"
            N_G = 1; // Число газовых проходов
            N_P = 1; // Число полей
            N_CK = 1; // Число секций
            A_H = 7.5; // Активная высота электродов 
            HK = 0.26; // Шаг между электродами
            R = 0.0011; // Эффективный радиус коронирующего электрода
            COR_EL = "гладкий"; // Тип коронирующего электрода
            HP = 0.1; // Расстояние между коронирующим и осадительным электродами
        }
        /// <summary>
        /// ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ПО УМОЛЧАНИЮ ДЛЯ ФИЛЬТРОВ МАРКИ "УВ"
        /// </summary>
        public void GetDefaultNotationOfUV()
        {
            // ПРИМЕЧАНИЯ К ЭЛЕКТРОФИЛЬТРАМ МАРКИ "УВ"
            N_G = 1; // Число газовых проходов
            N_P = 1; // Число полей
            HK = 0.275; // Шаг между одноимёнными электродами
            R = 0.002; // Эффективный радиус коронирующего электрода
            COR_EL = "игольчатый"; // Тип коронирующего электрода
            HP = 0.131; // Расстояние между коронирующим и осадительным электродами
        }

        /// <summary>
        /// ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ИЗ НАЗВАНИЯ ЭЛЕКТРОФИЛЬТРА МАРКИ "ЭГА"
        /// </summary>
        /// <param name="filterName">Название электрофильтра</param>
        public void GetDecryptionsOfNameFilterIGA(string filterName)
        {
            string[] filter = filterName.Split(new char[] { '-' });

            // List числовых значений, полученных из названия фильтра
            List<double> decryptionNumbers = new List<double>();

            bool isNameOfFilter = true;
            // Добавление значения в List
            foreach (string s in filter)
            {

                if (isNameOfFilter)
                {
                    FMark = s[0].ToString() + s[1].ToString() + s[2].ToString(); // Марка электрофильтра

                    string numSection = s[3].ToString();
                    double num = Convert.ToDouble(numSection);

                    decryptionNumbers.Add(num);
                }
                else
                {
                    double filterDecryption = Convert.ToDouble(s);
                    decryptionNumbers.Add(filterDecryption);
                }

                isNameOfFilter = false;
            }

            N_CK = Convert.ToDouble(decryptionNumbers[0]); // Число секций
            N_G = Convert.ToDouble(decryptionNumbers[1]); // Число газовых проходов
            A_H = Convert.ToDouble(decryptionNumbers[2]); // Активная высота электродов
            N_P = Convert.ToDouble(decryptionNumbers[4]); // Число полей

        }
        /// <summary>
        /// ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ИЗ НАЗВАНИЯ ЭЛЕКТРОФИЛЬТРА МАРКИ "ЭГТ"
        /// </summary>
        /// <param name="filterName">Название электрофильтра</param>
        public void GetDecryptionsOfNameFilterIGT(string filterName)
        {
            string[] filter = filterName.Split(new char[] { '-' });

            // List числовых значений, полученных из названия фильтра
            List<double> decryptionNumbers = new List<double>();

            bool isNameOfFilter = true;
            // Добавление значения в List
            foreach (string s in filter)
            {
                if (isNameOfFilter)
                {
                    FMark = s[0].ToString() + s[1].ToString() + s[2].ToString(); // Марка электрофильтра
                }
                else
                {
                    double filterDecryption = Convert.ToDouble(s);

                    decryptionNumbers.Add(filterDecryption);
                }

                isNameOfFilter = false;
            }

            N_P = Convert.ToDouble(decryptionNumbers[0]); // Число полей
            A_L = Convert.ToDouble(decryptionNumbers[1]); // Активная длина поля
            F_A = Convert.ToDouble(decryptionNumbers[2]); // Площадь активного сечения

        }
        /// <summary>
        /// ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ИЗ НАЗВАНИЯ ЭЛЕКТРОФИЛЬТРА МАРКИ "УВ"
        /// </summary>
        /// <param name="filterName">Название электрофильтра</param>
        public void GetDecryptionsOfNameFilterUV(string filterName)
        {
            string[] filter = filterName.Split(new char[] { '-' });

            // List числовых значений, полученных из названия фильтра
            List<double> decryptionNumbers = new List<double>();

            bool isNameOfFilter = true;
            // Добавление значения в List
            foreach (string s in filter)
            {
                if (isNameOfFilter == true)
                {
                    FMark = s.ToString(); // Марка электрофильтра
                }
                else if (isNameOfFilter == false)
                {
                    string numSection = s[0].ToString();
                    string activeHeight = s[2].ToString() + s[3].ToString();
                    double numS = Convert.ToDouble(numSection);
                    double activeH = Convert.ToDouble(activeHeight);

                    decryptionNumbers.Add(numS);
                    decryptionNumbers.Add(activeH);
                }

                isNameOfFilter = false;
            }

            N_CK = Convert.ToDouble(decryptionNumbers[0]); // Число секций
            A_H = Convert.ToDouble(decryptionNumbers[1]); // Активная высота электродов

        }

        /// <summary>
        /// ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ФИЛЬТРА МАРКИ "ЭГА"
        /// </summary>
        /// <param name="F_PA">Площадь активного сечения</param>
        public void SelectFilterOfIGA(double F_PA)
        {
            string query = "SELECT f_id, f_mark, f_active_length, f_square_active, f_square_total, f_overall_dim FROM IGA ORDER BY f_id";

            OleDbCommand command = new OleDbCommand(query, myConnection);
            OleDbDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                F_A = Convert.ToDouble(reader[3]);  // Площадь активного сечения

                if (F_A >= F_PA && F_PA >= 16.5) // 16.5 - минимальная площадь для фильтров "ЭГА"
                {
                    GetDefaultNotationOfIGA();

                    string filterName = reader[1].ToString();
                    GetDecryptionsOfNameFilterIGA(filterName);

                    A_L = Convert.ToDouble(reader[2]);  // Активная длина поля
                    F_OC = Convert.ToDouble(reader[4]); // Общая площадь осаждения

                    string[] filter = reader[5].ToString().Split(new char[] { 'х' });

                    F_L = Convert.ToDouble(filter[0]) * Math.Pow(10, -3); // Длина фильтра, м

                    // В данном случае округление реализовано специально для тестов (это не повлияет на конечный результат),
                    // поскольку при передаче значения в object происходит "баг".
                    // К примеру, значение "19.4" (19400) будет выглядеть как "19.0000000002"
                    F_H = Math.Round(Convert.ToDouble(filter[2]) * Math.Pow(10, -3), 1); // Высота фильтра, м
                    F_B = Convert.ToDouble(filter[1]) * Math.Pow(10, -3); // Ширина фильтра, м

                    break;
                }
                else
                {
                    F_A = 0;
                    HP = 0;
                }
            }

            reader.Close();
        }
        /// <summary>
        /// ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ФИЛЬТРА МАРКИ "ЭГТ"
        /// </summary>
        /// <param name="F_PA">Площадь активного сечения</param>
        public void SelectFilterOfIGT(double F_PA)
        {
            string query = "SELECT f_id, f_mark, f_square_total, f_overall_dim FROM IGT ORDER BY f_id";

            OleDbCommand command = new OleDbCommand(query, myConnection);
            OleDbDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                GetDecryptionsOfNameFilterIGT(reader[1].ToString());

                if (F_A >= F_PA && F_PA >= 20) // 20 - минимальная площадь для фильтров "ЭГТ"
                {
                    GetDefaultNotationOfIGT(); // Получаем значения из примечаний под таблицей (см. методичку)

                    F_OC = Convert.ToDouble(reader[2]); // Общая площадь осаждения

                    string[] filter = reader[3].ToString().Split(new char[] { 'x' });

                    F_L = Convert.ToDouble(filter[0]); // Длина фильтра, м
                    F_H = Convert.ToDouble(filter[2]); // Высота фильтра, м
                    F_B = Convert.ToDouble(filter[1]); // Ширина фильтра, м

                    break;
                }
                else
                {
                    FMark = "";
                    N_P = 0;
                    A_L = 0;
                    F_A = 0;
                    HP = 0;
                }
            }

            reader.Close();

        }
        /// <summary>
        /// ПОЛУЧЕНИЕ ЗНАЧЕНИЙ ФИЛЬТРА МАРКИ "УВ"
        /// </summary>
        /// <param name="F_PA">Площадь активного сечения</param>
        public void SelectFilterOfUV(double F_PA)
        {
            string query = "SELECT f_id, f_mark, f_square_active, f_square_total,  f_active_length, f_overall_dim FROM UV ORDER BY f_id";

            OleDbCommand command = new OleDbCommand(query, myConnection);
            OleDbDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                F_A = Convert.ToDouble(reader[2]);  // Площадь активного сечения

                if (F_A >= F_PA && F_PA >= 21) // 21 - минимальная площадь фильтров "УВ"
                {
                    GetDefaultNotationOfUV(); // Получаем значения из примечаний под таблицей (см. методичку)

                    string filterName = reader[1].ToString();
                    GetDecryptionsOfNameFilterUV(filterName);

                    A_L = Convert.ToDouble(reader[4]);  // Активная длина поля                    
                    F_OC = Convert.ToDouble(reader[3]); // Общая площадь осаждения

                    string[] filter = reader[5].ToString().Split(new char[] { 'х' });

                    F_L = Convert.ToDouble(filter[0]); // Длина фильтра, м
                    F_H = Convert.ToDouble(filter[2]); // Высота фильтра, м
                    F_B = Convert.ToDouble(filter[1]); // Ширина фильтра, м

                    break;
                }
                else
                {
                    F_A = 0;
                    HP = 0;
                }

            }

            reader.Close();
        }

        /// <summary>
        /// ВЫБОР ФИЛЬТРА
        /// </summary>
        public void GetSelectedFilter(double T_GAZ, double F_PA)
        {
            OpenConnection();

            // Максимально допустимые температуры газа для всех типов фильтров
            double T_MAX_IGA = 350;
            double T_MAX_IGT = 425;
            double T_MAX_UV = 250;

            if (T_GAZ <= T_MAX_UV)
            {
                SelectFilterOfUV(F_PA);

            }
            else if (T_GAZ <= T_MAX_IGA)
            {
                SelectFilterOfIGA(F_PA);

            }
            else if (T_GAZ <= T_MAX_IGT)
            {
                SelectFilterOfIGT(F_PA);
            }


        }

        /// <summary>
        /// ВЫБОР АГРЕГАТА ПИТАНИЯ
        /// </summary>
        public void SelectPowerUnitOfATG(double I_CUR)
        {
            string query = "SELECT p_id, p_mark, p_rectified_voltage, p_average_rectified_current, p_current, p_voltage, p_power, p_efficiency_factor, p_power_coefficient FROM ATF ORDER BY p_id";

            //// Инициализация подключения к базе данных Microsoft Excel
            //myConnection = new OleDbConnection(connectString);
            //myConnection.Open();
            OpenConnection();

            OleDbCommand command = new OleDbCommand(query, myConnection);
            OleDbDataReader reader = command.ExecuteReader();


            while (reader.Read())
            {
                if (Convert.ToDouble(reader[3]) >= I_CUR)
                {
                   PMark = reader[1].ToString();

                    string rectifiedVoltage = reader[2].ToString();
                    string[] rectifiedVoltageNumbers = rectifiedVoltage.Split(new char[] { '/' });

                    U_MAX = Convert.ToDouble(rectifiedVoltageNumbers[0]) * Math.Pow(10, 3);
                    U_U = Convert.ToDouble(rectifiedVoltageNumbers[1]) * Math.Pow(10, 3);

                    I_L = Convert.ToDouble(reader[3].ToString());
                    I_P = Convert.ToDouble(reader[4].ToString());

                    string voltage = reader[5].ToString();
                    string[] voltageNumbers = voltage.Split(new char[] { ',' });

                    U_C = Convert.ToDouble(voltageNumbers[0]);

                    N_C = Convert.ToDouble(reader[6].ToString());
                    P_AG = Convert.ToDouble(reader[7].ToString());
                    K_N = Convert.ToDouble(reader[8].ToString());
                    break;
                }
            }

            reader.Close();
        }
    }
}
