﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Сalculation
    {
        // КОНСТРУКТОР КЛАССА
        //--------------------
        public Сalculation() { }

        public Сalculation(double _V_RO, double _X, double _T_GAZ, double _P_BAR, double _P_GI, double _W, double _Z, double _I, List<double> _Q_J, double _D_V, double _P_EL)
        {
            V_RO = _V_RO;
            X = _X;
            T_GAZ = _T_GAZ;
            P_BAR = _P_BAR;
            P_GI = _P_GI;
            W = _W;
            Z = _Z;
            I = _I;
            Q_J = _Q_J;
            D_V = _D_V;
            P_EL = _P_EL;
        }

        // ИСХОДНЫЕ ЗНАЧЕНИЯ
        // --------------------------------------------
        /// <summary>
        /// Расход сухого газа на очистку при нормальных условиях
        /// </summary>
        public double V_RO;
        /// <summary>
        /// Содержание водяных паров в газе
        /// </summary>
        public double X;
        /// <summary>
        /// Температура очищаемого газа на входе
        /// </summary>
        public double T_GAZ;
        /// <summary>
        /// Барометрическое давление на местности
        /// </summary>
        public double P_BAR;
        /// <summary>
        /// Избыточное давление очищаемого газа
        /// </summary>
        public double P_GI;
        /// <summary>
        /// Рекомендуемая скорость очищаемого газа
        /// </summary>
        public double W;
        /// <summary>
        /// Заданный средний ток короны
        /// </summary>
        public double I;
        /// <summary>
        /// Концентрация пыли на входе
        /// </summary>
        public double Z;
        /// <summary>
        /// Динамическая вязкость газа при рабочих условиях
        /// </summary>
        public double D_V;
        /// <summary>
        /// Удельное электрическое сопротивление слоя пыли
        /// </summary>
        public double P_EL;
        /// <summary>
        /// Фракционный состав пыли
        /// </summary>
        public List<double> Q_J = new List<double>();

        // Некоторые постоянные значения, необходимые для расчётов
        public const double K_F = 1.4;   // Коэффициент формы кривой тока [1.1 - 1.4]
        public const double K_E = 1.412; // Коэффициент перехода от амплитудного значения напряжения к эффективному [1.405 - 1.412]

        // ПРОМЕЖУТОЧНЫЕ ЗНАЧЕНИЯ
        // --------------------------------------------
        /// <summary>
        /// Расход очищаемого газа в рабочих условиях
        /// </summary>
        public double V_GAZ;
        /// <summary>
        /// Площадь осаждения
        /// </summary>
        public double F_PA;

        // ЗНАЧЕНИЯ, ОТНОСЯЩИЕСЯ К ТИПУ ЭЛЕКТРОФИЛЬТРА
        // --------------------------------------------
        /// <summary>
        /// Марка электрофильтра
        /// </summary>
        public string FMark;
        /// <summary>
        /// Число секций
        /// </summary>
        public double N_CK;
        /// <summary>
        ///  Число газовых проходов
        /// </summary>
        public double N_G;
        /// <summary>
        /// Активная высота электродов
        /// </summary>
        public double A_H;
        /// <summary>
        /// Число полей
        /// </summary>
        public double N_P;
        /// <summary>
        ///  Активная длина поля
        /// </summary>
        public double A_L;
        /// <summary>
        /// Площадь активного сечения
        /// </summary>
        public double F_A;
        /// <summary>
        /// Общая площадь осаждения
        /// </summary>
        public double F_OC;
        /// <summary>
        /// Длина электрофильтра
        /// </summary>
        public double F_L;
        /// <summary>
        /// Высота электрофильтра
        /// </summary>
        public double F_H;
        /// <summary>
        /// Ширина электрофильтра
        /// </summary>
        public double F_B;
        /// <summary>
        /// Тип коронирующего электрода  
        /// </summary>
        public string COR_EL = "";
        /// <summary>
        /// Шаг между одноимёнными электродами
        /// </summary>
        public double HK;
        /// <summary>
        /// Эффективный радиус коронирующего электрода
        /// </summary>
        public double R;
        /// <summary>
        /// Расстояние между коронирующим и осадительным электродам
        /// </summary>
        public double HP;

        // РАСЧЁТНЫЕ ЗНАЧЕНИЯ ЭЛЕКТРОФИЛЬТРА
        // ---------------------------------------------

        /// <summary>
        /// Уточнённая скорость газа для электрофильтра
        /// </summary>
        public double W_U;
        /// <summary>
        /// Удельная осадительная поверхность
        /// </summary>
        public double F_F;
        /// <summary>
        /// Время пребывания газа в электрофильтре
        /// </summary>
        public double T_T;
        /// <summary>
        /// Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности
        /// </summary>
        public double E_KP;
        /// <summary>
        /// Критическое напряжение электрического поля у коронирующего электрода
        /// </summary>
        public double U_KP;
        /// <summary>
        /// Напряженность электрического поля вблизи поверхности осаждения пыли
        /// </summary>
        public double E_OC;
        /// <summary>
        /// Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций
        /// </summary>
        public List<double> W_W = new List<double>();
        /// <summary>
        /// Фракционная степень очистки газа
        /// </summary>
        public List<double> Z_Z = new List<double>();
        /// <summary>
        /// Общая аналитическая степень очистки газа
        /// </summary>
        public double ZZ_A;
        /// <summary>
        /// Физическая скорость дрейфа частиц
        /// </summary>
        public double W_E;
        /// <summary>
        /// Количество осаждаемой пыли
        /// </summary>
        public double M_OC;
        /// <summary>
        /// Количество уносимой из фильтра пыли
        /// </summary>
        public double M_YH;
        /// <summary>
        /// Рациональная пылеемкость осадительных электродов
        /// </summary>
        public double M_P;
        /// <summary>
        /// Рекомендуемое время между очередным встряхиванием осадительных электродов
        /// </summary>
        public double T_P;

        /// <summary>
        /// Сила тока, необходимая на питание одного поля электрофильтра  
        /// </summary>
        public double I_CUR;

        // ЗНАЧЕНИЯ, ОТНОСЯЩИЕСЯ К АГРЕГАТУ ПИТАНИЯ
        // --------------------------------------------
        /// <summary>
        /// Марка питания агрегата
        /// </summary>
        public string PMark;
        /// <summary>
        /// Выпрямленное напряжение (максимальное)
        /// </summary>
        public double U_MAX;
        /// <summary>
        /// Выпрямленное напряжение (среднее)
        /// </summary>
        public double U_U;
        /// <summary>
        /// Выпрямленный ток 
        /// </summary>
        public double I_L;
        /// <summary>
        /// Потребляемый ток 
        /// </summary>
        public double I_P;
        /// <summary>
        /// Напряжение сети
        /// </summary>
        public double U_C;
        /// <summary>
        /// Потребляемая из сети мощность
        /// </summary>
        public double N_C;
        /// <summary>
        /// КПД агрегата питания
        /// </summary>
        public double P_AG;
        /// <summary>
        /// Коэффициент мощности
        /// </summary>
        public double K_N;

        // РАСЧЁТНЫЕ ЗНАЧЕНИЯ АГРЕГАТА ПИТАНИЯ
        // ---------------------------------------------
        /// <summary>
        /// Расчетная потребляемая мощность агрегата на очистку газа
        /// </summary>
        public double N_R;
        /// <summary>
        /// Общая мощность, потребляемая агрегатом питания
        /// </summary>
        public double N_S;
        /// <summary>
        /// Количество агрегатов питания на электрофильтр 
        /// </summary>
        public double N_A;

        /// <summary>
        /// Экземпляр класса математической библиотеки
        /// </summary>
        EstimatedMethods EstimatedSample = new EstimatedMethods();

        /// <summary>
        /// РАСЧЁТ ВСЕХ ЗНАЧЕНИЙ
        /// </summary>
        public void GetAllValues()
        {
            DBConnectionClass DBConnectionSample = new DBConnectionClass();

            // Очистка некоторых значений электрофильтра и агрегата питания
            ReloadValues();

            // Очистка коллекций для возможности повторных расчётов 
            W_W.Clear();
            Z_Z.Clear();

            // РАСЧЁТ ПРОМЕЖУТОЧНЫХ ЗНАЧЕНИЙ
            // -----------------------------
            V_GAZ = EstimatedSample.GetExpenditureAmount(V_RO, X, T_GAZ, P_BAR, P_GI);  // Расход очищаемого газа в рабочих условиях
            F_PA = EstimatedSample.GetDepositionArea(V_GAZ, W);                         // Расчётная лощадь осаждения

            // ВЫБОР ТИПА ЭЛЕКТРОФИЛЬТРА
            // --------------------------
            DBConnectionSample.GetSelectedFilter(T_GAZ, F_PA);

            FMark = DBConnectionSample.FMark;
            N_CK = DBConnectionSample.N_CK;
            N_G = DBConnectionSample.N_G;
            A_H = DBConnectionSample.A_H;
            N_P = DBConnectionSample.N_P;
            A_L = DBConnectionSample.A_L;
            F_A = DBConnectionSample.F_A;
            F_OC = DBConnectionSample.F_OC;
            F_L = DBConnectionSample.F_L;
            F_H = DBConnectionSample.F_H;
            F_B = DBConnectionSample.F_B;
            COR_EL = DBConnectionSample.COR_EL;
            HK = DBConnectionSample.HK;
            R = DBConnectionSample.R;
            HP = DBConnectionSample.HP;

            // РАСЧЁТ ПАРАМЕТРОВ ЭЛЕКТРОФИЛЬТРА
            // --------------------------------
            W_U = EstimatedSample.GetUpdatedSpeed(V_GAZ, F_A);                // Уточнёная скорость газа для электрофильтра
            F_F = EstimatedSample.GetPrecipitationSurface(N_P, A_L, HP, W_U); // Удельная осадительная поверхность
            T_T = EstimatedSample.GetGasResidenceTime(A_L, W_U);              // Время пребывания газа в электрофильтре
            E_KP = EstimatedSample.GetCriticalFieldOfNegativeElectrode(T_GAZ, P_BAR, P_GI, R);  // Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности
            U_KP = EstimatedSample.GetCriticaVoltageOfElectrode(E_KP, R, HK, HP);  // Критическое напряжение электрического поля у коронирующего электрода
            E_OC = EstimatedSample.GetElectricFieldOfDepositionSurface(I);    // Напряженность электрического поля вблизи поверхности осаждения пыли

            for (int i = 0; i < 6; i++)
            {
                W_W.Add(EstimatedSample.GetPhysicalVelocityOnAnalyticalOfFractions(E_OC, Q_J[i], I, W_U, D_V, COR_EL)); // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций [6 значений]
            }

            for (int i = 0; i < 6; i++)
            {
                Z_Z.Add(EstimatedSample.GetFractionalDegreeOfPurification(N_P, W_W[i], A_L, W_U, HP)); // Фракционная степень очистки газа [6 значений]
            }

            ZZ_A = EstimatedSample.GetGeneralAnalyticalDegreeOfPurification(Z_Z); // Общая аналитическая степень очистки газа
            W_E = EstimatedSample.GetPhysicalSpeedOfParticles(W_W);               // Физическая скорость дрейфа частиц 
            M_OC = EstimatedSample.GetAmountPrecipitationOfDust(ZZ_A, Z, V_GAZ);  // Количество осаждаемой пыли
            M_YH = EstimatedSample.GetAmountRemovedOfDust(ZZ_A, Z, V_GAZ);        // Количество уносимой из фильтра пыли
            M_P = EstimatedSample.GetRationalDustOfElectrodes(P_EL);              // Рациональная пылеемкость осадительных электродов
            T_P = EstimatedSample.GetRecommendedTimeBetweenShakeOfElectrodes(F_OC, M_P, V_GAZ, Z, ZZ_A); //Рекомендуемое время между очередным встряхиванием осадительных электродов

            // ВЫБОР АГРЕГАТА ПИТАНИЯ
            // ----------------------
            I_CUR = EstimatedSample.GetRequiredCurrentToPowerOneFieldOfFilter(I, A_H, A_L, N_G, HK); // Сила тока, необходимая на питание одного поля электрофильтра

            DBConnectionSample.SelectPowerUnitOfATG(I_CUR);

            PMark = DBConnectionSample.PMark;
            U_MAX = DBConnectionSample.U_MAX;
            U_U = DBConnectionSample.U_U;
            I_L = DBConnectionSample.I_L;
            I_P = DBConnectionSample.I_P;
            U_C = DBConnectionSample.U_C;
            N_C = DBConnectionSample.N_C;
            P_AG = DBConnectionSample.P_AG;
            K_N = DBConnectionSample.K_N;

            // РАСЧЁТ ПАРАМЕТРОВ АГРЕГАТА ПИТАНИЯ
            // ----------------------------------
            N_R = EstimatedSample.GetEstimatePowerOfConsumption(I_CUR, I_L, U_U, U_MAX);    // Расчетная потребляемая мощность агрегата на очистку газа, кВа
            N_S = EstimatedSample.GetTotalPowerOfConsumption(U_MAX, I_L, K_F, K_E, K_N, P_AG, N_R); // Общая мощность, потребляемая агрегатом питания
            N_A = EstimatedSample.GetNumberOfAggregates(N_P); // Количество агрегатов питания на электрофильтр
        }


        /// <summary>
        /// МЕТОД, ОБНУЛЯЮЩИЙ ЗНАЧЕНИЯ ЭЛЕКТРОФИЛЬТРА И АГРЕГАТА ПИТАНИЯ
        /// </summary>
        public void ReloadValues()
        {
            HK = 0;  // Шаг между одноимёнными электродами
            R = 0; // Эффективный радиус коронирующего электрода, м

            FMark = ""; // Марка электрофильтра
            N_CK = 0; // Число секций
            N_G = 0;  // Число газовых проходов
            A_H = 0;  // Активная высота электродов
            N_P = 0;  // Число полей

            A_L = 0;  // Активная длина поля
            F_A = 0;  // Площадь активного сечения
            F_OC = 0; // Общая площадь осаждения

            F_L = 0; // Длина
            F_H = 0; // Высота
            F_B = 0; // Ширина

            //

            PMark = ""; // Марка питания агрегата
            U_MAX = 0;  // Выпрямленное напряжение (максимальное)
            U_U = 0;    // Выпрямленное напряжение (среднее)
            I_L = 0;    // Выпрямленный ток 
            I_P = 0;    // Потребляемый ток 
            U_C = 0;    // Напряжение сети 
            N_C = 0;    // Потребляемая из сети мощность
            P_AG = 0;   // КПД агрегата питания
            K_N = 0;    // Коэффициент мощности
        }

        public FieldInfo[] GetFieldInfo()
        {
            FieldInfo[] fields = typeof(Сalculation).GetFields();

            return fields;
        }
    }


    public class EstimatedMethods
    {
        // Полезные константы
        public const double PI = Math.PI;

        /// <summary>
        /// РАСХОД ОЧИЩАЕМОГО ГАЗА В РАБОЧИХ УСЛОВИЯХ (V_GAZ)
        /// </summary>
        /// <param name="V_RO">Расход сухого газа на очистку при нормальных условиях</param>
        /// <param name="X">Содержание водяных паров в газе</param>
        /// <param name="T_GAZ">Температура очищаемого газа на входе</param>
        /// <param name="P_BAR">Барометрическое давление на местности</param>
        /// <param name="P_GI">Избыточное давление очищаемого газа</param>
        /// <returns></returns>
        public double GetExpenditureAmount(double V_RO, double X, double T_GAZ, double P_BAR, double P_GI) 
        {
            return V_RO * (1 + (X * (Math.Pow(10, -3)) / 0.804)) * (T_GAZ + 273) * ((101.3) / (273 * (P_BAR + P_GI)));
        }

        /// <summary>
        /// ПЛОЩАДЬ ОСАЖДЕНИЯ (F_PA)
        /// </summary>
        /// <param name="V_GAZ">Расход очищаемого газа в рабочих условиях</param>
        /// <param name="W">Рекомендуемая скорость очищаемого газа</param>
        /// <returns></returns>
        public double GetDepositionArea(double V_GAZ, double W)
        {
            return V_GAZ / (3600 * W);
        }

        /// <summary>
        /// УТОЧНЁНАЯ СКОРОСТЬ ГАЗА ДЛЯ ЭЛЕКТРОФИЛЬТРА (W_U)
        /// </summary>
        /// <param name="V_GAZ">Расход очищаемого газа в рабочих условиях</param>
        /// <param name="F_A">Площадь активного сечения электрофильтра</param>
        /// <returns></returns>
        public double GetUpdatedSpeed(double V_GAZ, double F_A)
        {
            return V_GAZ / (3600 * F_A);
        }

        /// <summary>
        /// УДЕЛЬНАЯ ОСАДИТЕЛЬНАЯ ПОВЕРХНОСТЬ (F_F)
        /// </summary>
        /// <param name="N_P">Число полей в электрофильтре</param>
        /// <param name="A_L">Активная длина поля электрофильтра</param>
        /// <param name="H_P">Расстояние между коронирующим и осадительным электродами в электрофильтре</param>
        /// <param name="W_U">Уточнёная скорость газа для электрофильтра</param>
        /// <returns></returns>
        public double GetPrecipitationSurface(double N_P, double A_L, double H_P, double W_U)
        {
            return (N_P * A_L) / (H_P * W_U);
        }

        /// <summary>
        /// ВРЕМЯ ПРЕБЫВАНИЯ ГАЗА В ЭЛЕКТРОФИЛЬТРЕ (T_T)
        /// </summary>
        /// <param name="A_L">Активная длина поля электрофильтра</param>
        /// <param name="W_U">Уточнёная скорость газа для электрофильтра</param>
        /// <returns></returns>
        public double GetGasResidenceTime(double A_L, double W_U)
        {
            return A_L / W_U;
        }

        /// <summary>
        /// КРИТИЧЕСКАЯ НАПРЯЖЕННОСТЬ ЭЛЕКТРИЧЕСКОГО ПОЛЯ ДЛЯ КОРОНИРУЮЩЕГО ЭЛЕКТРОДА ОТРИЦАТЕЛЬНОЙ ПОЛЯРНОСТИ (E_KP)
        /// </summary>
        /// <param name="T_GAZ">Температура очищаемого газа на входе</param>
        /// <param name="P_BAR">Барометрическое давление на местности</param>
        /// <param name="P_GI">Избыточное давление очищаемого газа</param>
        /// <param name="R">Эффективный радиус коронирующего электрода в электрофильтре</param>
        /// <returns></returns>
        public double GetCriticalFieldOfNegativeElectrode(double T_GAZ, double P_BAR, double P_GI, double R)
        {
            double B = 273 / (101.3 * (T_GAZ + 273)) * (P_BAR + P_GI); // Переменная для упрощения (повторяющееся значение)

            return 3.039 * (Math.Pow(10, 6)) * (B + 0.0311 * Math.Sqrt(B / R));
        }

        /// <summary>
        /// КРИТИЧЕСКОЕ НАПРЯЖЕНИЕ ЭЛЕКТРИЧЕСКОГО ПОЛЯ У КОРОНИРУЮЩЕГО ЭЛЕКТРОДА (U_KP)
        /// </summary>
        /// <param name="E_KP">Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности</param>
        /// <param name="R">Эффективный радиус коронирующего электрода в электрофильтре</param>
        /// <param name="H_K">Шаг между одноимёнными электродами в электрофильтре</param>
        /// <param name="H_P">Расстояние между коронирующим и осадительными электродами</param>
        /// <returns></returns>
        public double GetCriticaVoltageOfElectrode(double E_KP, double R, double H_K, double H_P)
        {
            double D = 0; // Вспомогательная переменная
            double condition = H_P / H_K; // Вспомогательная переменная

            if (condition >= 1)
            {
                D = (H_K / 2) * Math.Exp((PI * H_P) / H_K);

            }
            else if (condition < 1)
            {

                D = (4 * H_P) / PI;
            }

            return E_KP * R * Math.Log(D / R);
        }

        /// <summary>
        /// НАПРЯЖЕННОСТЬ ЭЛЕКТРИЧЕСКОГО ПОЛЯ ВБЛИЗИ ПОВЕРХНОСТИ ОСАЖДЕНИЯ ПЫЛИ (E_OC)
        /// </summary>
        /// <param name="I">Заданный средний ток короны для электрофильтра</param>
        /// <returns></returns>
        public double GetElectricFieldOfDepositionSurface(double I)
        {
            return 5.48 * (Math.Pow(10, 5)) * Math.Sqrt(I);
        }

        /// <summary>
        /// ФИЗИЧЕСКАЯ СКОРОСТЬ ДРЕЙФА ЧАСТИЦ ПО АНАЛИТИЧЕСКИМ ДАННЫМ ДЛЯ ЗАДАННЫХ ФРАКЦИЙ (W_W)
        /// </summary>
        /// <param name="E_OC">Напряженность электрического поля вблизи поверхности осаждения пыли</param>
        /// <param name="Q_J">Фракционный состав пыли</param>
        /// <param name="I">Заданный средний ток короны для электрофильтра</param>
        /// <param name="W_U">Уточнёная скорость газа для электрофильтра</param>
        /// <param name="D_V">Динамическая вязкость газа при рабочих условиях</param>
        /// <param name="COR_EL">Тип коронирующего электрода в электрофильтре</param>
        /// <returns></returns>
        public double GetPhysicalVelocityOnAnalyticalOfFractions(double E_OC, double Q_J, double I, double W_U, double D_V, string COR_EL)
        {
            // Коэффициенты дрейфа
            double a_1;
            double a_2;

            // Условие на тип коронирующего электрода
            if (COR_EL == "игольчатый")
            {
                a_1 = 3;
                a_2 = 0.2;

            }
            else
            {
                a_1 = 9;
                a_2 = 0.2;
            }

            // Изначально первый множитель был равен 0.816, что расходилось с результатами контрольного примера (!)
            return 8.16 * E_OC * Math.Sqrt(Q_J * (a_1 * Math.Sqrt(I * Math.Pow(10, -3)) * a_2 * W_U) * Math.Pow(10, -6) * ((1 / (4 * PI * 9 * Math.Pow(10, 9))) / (D_V)));
        }

        /// <summary>
        /// ФРАКЦИОННАЯ СТЕПЕНЬ ОЧИСТКИ ГАЗА (Z_Z)
        /// </summary>
        /// <param name="N_P">Число полей в электрофильтре</param>
        /// <param name="W_W">Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций</param>
        /// <param name="A_L">Активная длина поля электрофильтра</param>
        /// <param name="W_U">Уточнёная скорость газа в электрофильтре</param>
        /// <param name="H_P">Расстояние между коронирующим и осадительным электродами</param>
        /// <returns></returns>
        public double GetFractionalDegreeOfPurification(double N_P, double W_W, double A_L, double W_U, double H_P)
        {           
            return 1 - Math.Exp(-N_P * W_W * A_L / (W_U * H_P)); // (!) Расходится со значениями контрольного примера
        }

        /// <summary>
        /// ОБЩАЯ АНАЛИТИЧЕСКАЯ СТЕПЕНЬ ОЧИСТКИ ГАЗА (ZZ_A)
        /// </summary>
        /// <param name="Z_Z">Фракционная степень очистки газа</param>
        /// <returns></returns>
        public double GetGeneralAnalyticalDegreeOfPurification(List<double> Z_Z)
        {
            // Вспомогательная переменная
            double summ = 0;

            // Принято решение расчёта среднего значения:
            for (int i = 0; i < Z_Z.Count; i++)
            {
                summ += Z_Z[i];
            }

            return summ / Z_Z.Count;
        }

        /// <summary>
        /// ФИЗИЧЕСКАЯ СКОРОСТЬ ДРЕЙФА ЧАСТИЦ (W_E)
        /// </summary>
        /// <param name="W_W">Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций</param>
        /// <returns></returns>
        public double GetPhysicalSpeedOfParticles(List<double> W_W)
        {
            // Вспомогательная переменная
            double summ = 0;

            for (int i = 0; i < W_W.Count; i++)
            {
                summ += W_W[i];
            }

            return summ / W_W.Count;
        }

        /// <summary>
        /// КОЛИЧЕСТВО ОСАЖДАЕМОЙ ПЫЛИ В ЭЛЕКТРОФИЛЬТРЕ (M_OC)
        /// </summary>
        /// <param name="ZZ_A">Общая аналитическая степень очистки газа</param>
        /// <param name="Z">Концентрация пыли на входе</param>
        /// <param name="V_GAZ">Расход очищаемого газа в рабочих условиях</param>
        /// <returns></returns>
        public double GetAmountPrecipitationOfDust(double ZZ_A, double Z, double V_GAZ)
        {
            return Math.Pow(10, -3) * ZZ_A * Z * V_GAZ;
        }

        /// <summary>
        /// КОЛИЧЕСТВО УНОСИМОЙ ИЗ ФИЛЬТРА ПЫЛИ (M_YH)
        /// </summary>
        /// <param name="ZZ_A">Общая аналитическая степень очистки газа</param>
        /// <param name="Z">Концентрация пыли на входе</param>
        /// <param name="V_GAZ">Расход очищаемого газа в рабочих условиях</param>
        /// <returns></returns>
        public double GetAmountRemovedOfDust(double ZZ_A, double Z, double V_GAZ)
        {
            return Math.Pow(10, -3) * (1 - ZZ_A) * Z * V_GAZ;
        }

        /// <summary>
        /// РАЦИОНАЛЬНАЯ ПЫЛЕЁМКОСТЬ ОСАДИТЕЛЬНЫХ ЭЛЕКТРОДОВ В ЭЛЕКТРОФИЛЬТРЕ (M_P)
        /// </summary>
        /// <param name="P_EL">Удельное электрическое сопротивление слоя пыли</param>
        /// <returns></returns>
        public double GetRationalDustOfElectrodes(double P_EL)
        {
            return 3.2 - 0.267 * Math.Log10(P_EL);
        }

        /// <summary>
        /// РЕКОМЕНДУЕМОЕ ВРЕМЯ МЕЖДУ ОЧЕРЕДНЫМ ВСТРЯХИВАНИЕМ ОСАДИТЕЛЬНЫХ ЭЛЕКТРОДОВ В ЭЛЕКТРОФИЛЬТРЕ (T_P)
        /// </summary>
        /// <param name="F_OC">Общая площадь осаждения электрофильтра</param>
        /// <param name="M_P">Рациональная пылеемкость осадительных электродов</param>
        /// <param name="V_GAZ">Расход очищаемого газа в рабочих условиях</param>
        /// <param name="Z">Концентрация пыли на входе</param>
        /// <param name="ZZ_A">Общая аналитическая степень очистки газа</param>
        /// <returns></returns>
        public double GetRecommendedTimeBetweenShakeOfElectrodes(double F_OC, double M_P, double V_GAZ, double Z, double ZZ_A)
        {
            return (16.7 * F_OC * M_P) / (V_GAZ * Z * ZZ_A);
        }

        /// <summary>
        /// СИЛА ТОКА, НЕОБХОДИМАЯ НА ПИТАНИЕ ОДНОГО ПОЛЯ ЭЛЕКТРОФИЛЬТРА (I_CUR)
        /// </summary>
        /// <param name="I">Заданный средний ток короны для электрофильтра</param>
        /// <param name="A_H">Активная высота электродов в электрофильтре</param>
        /// <param name="A_L">Активная длина поля в электрофильтре</param>
        /// <param name="N_G">Число газовых проходов в электрофильтре</param>
        /// <param name="HK">Шаг между одноимёнными электродами в электрофильтре</param>
        /// <returns></returns>
        public double GetRequiredCurrentToPowerOneFieldOfFilter(double I, double A_H, double A_L, double N_G, double HK)
        {
            return I * ((A_H * A_L * N_G) / (HK));
        }

        /// <summary>
        /// РАСЧЁТНАЯ ПОТРЕБЛЯЕМАЯ МОЩНОСТЬ АГРЕГАТА НА ОЧИСТКУ ГАЗА (N_R)
        /// </summary>
        /// <param name="I_CUR">Сила тока, необходимая на питание одного поля электрофильтра</param>
        /// <param name="I_L">Выпрямленный ток агрегата питания</param>
        /// <param name="U_U">Среднее выпрямленное напряжение агрегата питания</param>
        /// <param name="U_MAX">Максимальное выпрямленное напряжение агрегата питания</param>
        /// <returns></returns>
        public double GetEstimatePowerOfConsumption(double I_CUR, double I_L, double U_U, double U_MAX)
        {
            // Вспомогательные переменные
            double N_NOM = U_U * I_L * Math.Pow(10, -6); // Номинальная мощность
            double K_I = I_CUR / I_L; // Коэффициент загрузки агрегата по току
            double K_U = U_MAX / U_U; // Коэффициент загрузки агрегата по напряжению

            return K_I * K_U * N_NOM;
        }

        /// <summary>
        /// ОБЩАЯ МОЩНОСТЬ, ПОТРЕБЛЯЕМАЯ АГРЕГАТОМ ПИТАНИЯ (N_S)
        /// </summary>
        /// <param name="U_MAX">Максимальное выпрямленное напряжение агрегата питания</param>
        /// <param name="I_L">Выпрямленный ток агрегата питания</param>
        /// <param name="K_F">Коэффициент формы кривой тока [1.1 - 1.4]</param>
        /// <param name="K_E">Коэффициент перехода от амплитудного значения напряжения к эффективному [1.405 - 1.412]</param>
        /// <param name="K_N">Коэффициент мощности агрегата питания</param>
        /// <param name="P_AG">КПД агрегата питания</param>
        /// <param name="N_R">Расчётная потребляемая мощность агрегата на очистку газа</param>
        /// <returns></returns>
        public double GetTotalPowerOfConsumption(double U_MAX, double I_L, double K_F, double K_E, double K_N, double P_AG, double N_R)
        {
            double N_ALL = 0.05 * N_R;
            return (U_MAX * I_L * K_F * K_N / (K_E * P_AG) + N_ALL) * Math.Pow(10, -6);
        }

        /// <summary>
        /// КОЛИЧЕСТВО АГРЕГАТОВ ПИТАНИЯ НА ЭЛЕКТРОФИЛЬТР (N_A)
        /// </summary>
        /// <param name="N_P">Число полей в электрофильтре</param>
        /// <returns></returns>
        public double GetNumberOfAggregates(double N_P)
        {
            return N_P;
        }
    }
}
